package com.example.targets;

import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.spring.test.ApplicationContextMock;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import com.example.WicketApplication;
import com.example.services.DefaultService;
import com.example.services.IService;
import com.example.targets.Page5;

public class Page5Test {

	@Test
	public void 通常ではSpringServiceがインジェクションされる() {
		WicketTester tester = new WicketTester(new WicketApplication());
		tester.startPage(Page5.class);
		tester.assertRenderedPage(Page5.class);
		tester.assertLabel("sayHello", "Hello, I am dependency injected object.");
	}

	@Test
	public void DefaultServiceがインジェクションされる() {
		WicketTester tester = new WicketTester(new WicketApplication() {
			@Override
			public void initSpring() {
				IService service = new DefaultService();
				ApplicationContextMock ctxm = new ApplicationContextMock();
				ctxm.putBean("service", service);
				getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctxm));
			}
		});
		tester.startPage(Page5.class);
		tester.assertRenderedPage(Page5.class);
		tester.assertLabel("sayHello", "Hello, I am instanced object.");
	}

}
