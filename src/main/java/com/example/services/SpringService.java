package com.example.services;

import org.springframework.stereotype.Service;

@Service
public class SpringService implements IService {

	@Override
	public String getHello() {
		return "Hello, I am dependency injected object.";
	}

}
