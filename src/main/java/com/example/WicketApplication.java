package com.example;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.wicketstuff.annotation.scan.AnnotatedMountScanner;

import com.example.targets.Page2;

public class WicketApplication extends WebApplication {
	@Override
	public Class<? extends WebPage> getHomePage() {
		return HomePage.class;
	}

	@Override
	public void init() {
		super.init();
		getRequestCycleSettings().setResponseRequestEncoding("UTF-8");
		getMarkupSettings().setDefaultMarkupEncoding("UTF-8");

		// BookmarkablePageLink
		mountPage("/page2", Page2.class);
		// Wicket-Stuff-Annotation
		new AnnotatedMountScanner().scanPackage("com.example").mount(this);
		// Wicket-Spring
		initSpring();
	}

	public void initSpring() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		// アノテーション付きのBeanを下のパッケージから検索する
		ctx.scan("com.example");
		ctx.refresh();
		getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx));
	}
}
