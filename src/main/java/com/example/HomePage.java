package com.example;

import lombok.val;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.example.targets.Page1;
import com.example.targets.Page2;
import com.example.targets.Page3;
import com.example.targets.Page4;
import com.example.targets.Page5;

public class HomePage extends WebPage {

	public HomePage() {

		val p1aModel = new Model<Integer>(42);
		val p1bModel = new Model<Integer>(67);
		add(new Link<Void>("toP1") {
			@Override
			public void onClick() {
				setResponsePage(new Page1(p1aModel, p1bModel));
			}
		});

		PageParameters p2Parms = new PageParameters();
		p2Parms.add("p2a", 55);
		p2Parms.add("p2b", 66);
		add(new BookmarkablePageLink<Void>("toP2", Page2.class, p2Parms));

		PageParameters p3Parms = new PageParameters();
		p3Parms.add("p3a", 82);
		p3Parms.add("p3b", 99);
		add(new BookmarkablePageLink<Void>("toP3", Page3.class, p3Parms));

		add(new BookmarkablePageLink<Void>("toP4", Page4.class));

		add(new BookmarkablePageLink<Void>("toP5", Page5.class));
	}
}
