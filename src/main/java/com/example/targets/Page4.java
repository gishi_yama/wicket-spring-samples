package com.example.targets;

import lombok.val;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.wicketstuff.annotation.mount.MountPath;

import com.example.services.DefaultService;
import com.example.services.IService;

@MountPath(value = "/page4")
public class Page4 extends WebPage {

	public Page4() {
		IService service = new DefaultService();

		val helloModel = Model.of(service.getHello());
		add(new Label("sayHello", helloModel));

	}
}
