package com.example.targets;

import lombok.val;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class Page1 extends WebPage {

	public Page1(IModel<Integer> p1aModel, Model<Integer> p1bModel) {
		add(new Label("p1a", p1aModel));
		add(new Label("p1b", p1bModel));

		val sumModel = Model.of(p1aModel.getObject() + p1bModel.getObject());
		add(new Label("sum", sumModel));
	}

}
