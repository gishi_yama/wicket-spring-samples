package com.example.targets;

import lombok.val;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath(value = "/page3", alt = { "/page3/${p3a}/${p3b}" })
public class Page3 extends WebPage {
	public Page3(PageParameters params) {
		val p3aModel = Model.of(params.get("p3a").toInt(0));
		val p3bModel = Model.of(params.get("p3b").toInt(0));
		add(new Label("p3a", p3aModel));
		add(new Label("p3b", p3bModel));

		val sumModel = Model.of(p3aModel.getObject() + p3bModel.getObject());
		add(new Label("sum", sumModel));
	}
}
