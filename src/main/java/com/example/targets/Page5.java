package com.example.targets;

import lombok.val;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.annotation.mount.MountPath;

import com.example.services.IService;

@MountPath(value = "/page5")
public class Page5 extends WebPage {

	@SpringBean
	private IService service;

	public Page5() {
		val helloModel = Model.of(service.getHello());
		add(new Label("sayHello", helloModel));
	}

}
