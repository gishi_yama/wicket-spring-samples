package com.example.targets;

import lombok.val;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class Page2 extends WebPage {

	public Page2(PageParameters params) {
		val p2aModel = Model.of(params.get("p2a").toInt(0));
		val p2bModel = Model.of(params.get("p2b").toInt(0));
		add(new Label("p2a", p2aModel));
		add(new Label("p2b", p2bModel));

		val sumModel = Model.of(p2aModel.getObject() + p2bModel.getObject());
		add(new Label("sum", sumModel));
	}
}
